#include <stdio.h>
#include <stdlib.h>

#include <jansson.h>

int main() {
  json_t* root = json_loadf(stdin, 0, NULL);

  if(!json_is_array(root))
    return 1;

  size_t index;
  json_t *value;

  json_array_foreach(root, index, value) {
    json_t* cca3_json = json_object_get(value, "cca3");

    json_t* capitales_json = json_object_get(value, "capital");
    json_t* capitale_json = json_array_get(capitales_json, 0);

    const char* cca3 = json_string_value(cca3_json);
    const char* capitale = json_string_value(capitale_json);

    printf("%s,%s\n", cca3, capitale);
  }

  return 0;
}
