CC     = gcc
CFLAGS = $(if $(DEBUG),-ggdb -O0,-O2) -Wall -Wextra $(shell pkg-config --cflags cairo)
LFLAGS = $(shell pkg-config --libs jansson cairo)
EXECS  = country jansson cairo

all: $(EXECS)

country: country.o treemap.o
jansson: jansson.o
cairo: cairo.o

%: %.o
	$(CC) $^ -o $@ $(LFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean

clean:
	rm -f *.o $(EXECS)

#  vim: set ts=4 sw=4 tw=120 noet :
