#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "treemap.h"

#define BUF_SIZE 500

int main() {
  TreeMap t = treemapCreate();
  char buffer[BUF_SIZE];
  while(fgets(buffer, BUF_SIZE, stdin)) {
    char* cca3 = strtok(buffer, ",");
    char* capitale = strtok(NULL, "\n");
    treemapSet(&t, cca3, capitale);
  }

  treemapPrint(&t);

  treemapDelete(&t);
}
