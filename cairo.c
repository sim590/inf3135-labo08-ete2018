#include <stdio.h>
#include <stdlib.h>
#include <cairo.h>

#define OUTPUT_FILE "image.png"
#define OFFSET 5

int main(int argc, char* argv[]) {
  // Validation des paramètres
  if(argc != 4) {
    printf("USAGE: %s NB_LINES NB_COLS SIDE_LEN\n", argv[0]);
    return -1;
  }
  int nLines = strtol(argv[1], NULL, 10);
  int nCols = strtol(argv[2], NULL, 10);
  int sideLength = strtol(argv[3], NULL, 10);

  // Construction de l'objet Cairo
  int height = nLines * (sideLength + OFFSET);
  int length = nCols * (sideLength + OFFSET);
  cairo_surface_t* surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, length, height);
  cairo_t* cr = cairo_create (surface);

  // Background de l'image
  cairo_set_source_rgb (cr, 255, 255, 255);
  cairo_rectangle (cr, 0, 0, length, height);
  cairo_fill (cr);

  cairo_set_line_width (cr, 1);
  cairo_set_source_rgb (cr, 255, 0, 0);

  // Dessin des cases
  for(int i = 0; i < nLines; ++i) {
      for(int j = 0; j < nCols; ++j) {
          // Top
          cairo_move_to(cr, j * (sideLength + OFFSET), i * (sideLength + OFFSET));
          cairo_line_to(cr, (j+1) * (sideLength + OFFSET), i * (sideLength + OFFSET));
          cairo_stroke(cr);

          // Left
          cairo_move_to(cr, j * (sideLength + OFFSET), i * (sideLength + OFFSET));
          cairo_line_to(cr, j * (sideLength + OFFSET), (i+1) * (sideLength + OFFSET));
          cairo_stroke(cr);

          // Right
          cairo_move_to(cr, (j+1) * (sideLength + OFFSET), i * (sideLength + OFFSET));
          cairo_line_to(cr, (j+1) * (sideLength + OFFSET), (i+1) * (sideLength + OFFSET));
          cairo_stroke(cr);

          // Bottom
          cairo_move_to(cr, j * (sideLength + OFFSET), (i+1) * (sideLength + OFFSET));
          cairo_line_to(cr, (j+1) * (sideLength + OFFSET), (i+1) * (sideLength + OFFSET));
          cairo_stroke(cr);
      }
  }

  // Création de l'image
  cairo_surface_write_to_png(surface, OUTPUT_FILE);

  // Libération de la mémoire
  cairo_destroy(cr);
  cairo_surface_destroy(surface);

  return 0;
}
